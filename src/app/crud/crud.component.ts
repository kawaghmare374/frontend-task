import { Component, OnInit } from '@angular/core';
import { TaskServicesService } from '../task-services.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {
  currentproduct = null;

  taskUpdate ={
    Title: "",
     Description: "", 
     Date: "",
      assignesTo: "", 
      Priority: "",
       Duration: "",
       available: false
  
  };


  
  submitted = false;
  constructor(private Taskservice: TaskServicesService, private formBuilder: FormBuilder, private router: Router) {
    this.getAllTask()


   }

  ngOnInit(): void {
  }

  TaskDetail: any;
  title = 'taskdashboard';
  data: any;
  Title: any;
  Description: any;
  Date: any;
  assignesTo: any;
  Priority: any;
  Duration: any;
  showModal: boolean = false;
  model: any = {};
  task: any;
  id: any;

  employeeDetail : any;
  status: any;
 


  getAllTask() {
    this.Taskservice.getAllTask().subscribe(res => {
      this.data = res;
      //console.log(res);

      this.TaskDetail = this.data.data.task;

      //console.log(this.TaskDetail);

    }, err => {
      console.log(err)
    })
  }

  openModel() {
    alert("halllo")
    this.showModal = true;
  }



  //////////////////


  createTask() {

alert("increatetask");

    let Taskdata = { "Title": this.Title, "Description": this.Description, "Date": this.Date, "assignesTo": this.assignesTo, "Priority": this.Priority, "Duration": this.Duration };
    console.log(Taskdata);
    

    this.Taskservice.createTask(Taskdata).subscribe(res => {
      console.log(res);
      alert(res);


       this.getAllTask();
      // console.log(data);

      this.submitted = true;
    }, err => {
      alert(err);
      console.log(err);
    })
  }

  deleteTask(id: any){
    alert(id);
    this.Taskservice.deleteTask(id).subscribe(res => {
      alert(res);
      this.getAllTask();
    }, err => {
      alert('something went wrong');
    })
  }


  edit(Task: any){
    this.taskUpdate = Task;
  }

  update(id: any){
    alert(id);
    
    this.Taskservice.updateTask(this.TaskDetail._id).subscribe(
      (resp) => {
        alert('updated success');
        this.getAllTask();
        console.log(resp);
      },
      
      (err) => {
        console.log(err);
        
      alert('something went wrong')
      }
    );
  }



}
