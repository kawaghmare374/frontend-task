import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
 import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel lives here
import { Router, RouterModule ,Routes} from '@angular/router';

import { CrudComponent } from './crud/crud.component';
import { TaskserviceComponent } from './taskservice/taskservice.component';
import { HeadingComponent } from './heading/heading.component';




const routes: Routes=[

  {path : 'form', component:HeadingComponent},
  {path :'Cancle',component:HeadingComponent}
 
  
]; 


@NgModule({
  declarations: [
    AppComponent,
   
    CrudComponent,
    TaskserviceComponent,
    HeadingComponent,
  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    ReactiveFormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
