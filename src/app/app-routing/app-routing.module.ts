import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CrudComponent } from '../crud/crud.component';


const routes: Routes = [
  { path: '', redirectTo: 'task', pathMatch: 'full' },

  { path: 'task/:id', component: CrudComponent },

];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    [RouterModule.forRoot(routes)],
    
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
