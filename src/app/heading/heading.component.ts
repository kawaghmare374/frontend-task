import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder }  from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heading',
  templateUrl: './heading.component.html',
  styleUrls: ['./heading.component.css']
})
export class HeadingComponent implements OnInit {
  editForm:FormGroup;

  constructor(private route :Router , fb: FormBuilder) { 
    this.editForm = fb.group({
      name: ["", Validators.required]
  });


   }

  ngOnInit(): void {
  }

}
