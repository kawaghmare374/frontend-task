import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TaskServicesService {

  constructor(private http:HttpClient) { }

  getAllTask(){
    return this.http.get('http://localhost:5001/api/v1/Task/getalltask');
}

 createTask(Taskdata: any){
  alert("in services")
  console.log("In services");
  
  console.log(Taskdata);
  
  return this.http.post('http://localhost:5001/api/v1/Task/createnewTask',Taskdata);
} 

 deleteTask(id: any){
  let url = `http://localhost:5001/api/v1/Task/deleteTask/${id}`;
  console.log(url, "delete url is here");   
  return this.http.delete(`http://localhost:5001/api/v1/Task/deleteTask/${id}`);
}

updateTask(id: any){
  console.log(id);
  
  alert("in services")
  console.log("In services");

  
  let url = `http://localhost:5001/api/v1/Task/updateTask/${id}`;
  console.log(url, 'updated url here');
  return this.http.patch(url,id);  

 

} 
}
